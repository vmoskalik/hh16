package sixteen.task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.*;

import static sixteen.task2.ListOperations.decrementList;
import static sixteen.task2.ListOperations.incrementList;
import static sixteen.task2.ListOperations.listToBigInt;

/**
 * Бесконечная последовательность
 *
 * @author Vitaly Moskalik
 *         created on 04.10.2016
 */
public class InfinitySequence {

    // все листы используются для представления чисел в виде отдельных цифр
    private static List<Byte> enteredSeq = new ArrayList<>(); // введенная строка
    private static List<Byte> initialList = new ArrayList<>(); // лист, который взят за отправную точку
    private static List<Byte> workList = new ArrayList<>(); // рабочий лист
    // числа, прошедшие проверку (все - одной разрядности)
    private static TreeMap<BigInteger, Integer> candidates = new TreeMap<>(); //key - candidate, value - shift

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                String s = reader.readLine();
                if (s.trim().isEmpty()) {
                    return;
                }

                initEnteredSeq(s);
                check();

                if (!reader.ready()) {
                    return;
                }
            }
        }
    }

    // инициализация enteredSeq
    private static void initEnteredSeq(final String s) throws IllegalArgumentException {
        enteredSeq.clear();
        if (s == null || s.trim().length() == 0) {
            throw new IllegalArgumentException("String is Null or Empty");
        }
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (Character.isDigit(ch)) {
                enteredSeq.add((byte) (ch - '0'));
            } else {
                throw new IllegalArgumentException(String.format("Illegal character at index %s: %s", i, s));
            }
        }
    }

    // основной метод проверки
    private static void check() {
        // if (enteredSeq contains only zeros)
        if (listToBigInt(enteredSeq).equals(BigInteger.ZERO)) {
            getAnswer(BigInteger.valueOf(10).pow(enteredSeq.size()), -1, enteredSeq.size() + 1);
            return;
        }

        for (int i = 1; i <= enteredSeq.size(); i++) { //capacity
            for (int j = 0; j < i; j++) { //shift
                findCandidates(i, j);
            }
            if (!candidates.isEmpty()) {
                Map.Entry<BigInteger, Integer> entry = candidates.firstEntry();
                getAnswer(entry.getKey(), entry.getValue(), i);
                return;
            }
        }
    }

    // поиск кандидатов
    private static void findCandidates(final int capacity, final int shift) {
        if (!initialise(capacity, shift)) {
            return;
        }
        if (shift > 0) {
            if (!checkPrevious(shift)) {
                return;
            }
        }
        if (!checkNext(shift + capacity)) {
            return;
        }
        candidates.put(listToBigInt(initialList), shift);
    }

    // инициализация initialList
    private static boolean initialise(final int capacity, final int shift) {
        initialList.clear();
        for (int i = 0; i < capacity; i++) {
            try {
                Byte b = enteredSeq.get(i + shift);
                if (initialList.size() == 0 && b == 0) {
                    return false; // ведущие нули не допускаются
                }
                initialList.add(b);
            } catch (IndexOutOfBoundsException e) {
                int toAddCount = capacity - initialList.size();

                workList.clear();
                for (int j = 0; j < toAddCount; j++) { //заполняем workList нужным кол-вом цифр
                    workList.add(enteredSeq.get(shift - toAddCount + j));
                }
                incrementList(workList);

                for (int j = workList.size() - toAddCount; j < workList.size(); j++) {
                    initialList.add(workList.get(j));
                }
                return true;
            }
        }
        return true;
    }

    // проверка числа, предшествующего initialList
    private static boolean checkPrevious(final int shift) {
        workList.clear();
        workList.addAll(initialList);

        decrementList(workList);
        for (int i = 0; i < shift; i++) {
            if (workList.get(workList.size() - i - 1).byteValue() != enteredSeq.get(shift - i - 1).byteValue()) {
                return false;
            }
        }
        return true;
    }

    // проверка последующих чисел
    private static boolean checkNext(int index) {
        if (index >= enteredSeq.size()) {
            return true;
        }
        workList.clear();
        workList.addAll(initialList);

        while (true) {
            incrementList(workList);

            for (Byte b : workList) {
                if (b.byteValue() != enteredSeq.get(index).byteValue()) {
                    return false;
                }
                index++;
                if (index == enteredSeq.size()) {
                    return true;
                }
            }
        }
    }

    // получение ответа путем подсчета кол-ва символов, предшествующих number (с учетом shift) в бесконечной последовательности
    private static void getAnswer(final BigInteger number, final int shift, final int capacity) {
        candidates.clear();
        BigInteger result = BigInteger.ZERO;

        //подсчет кол-ва символов из всех чисел, разрядность которых меньше, чем у number
        for (int i = 1; i < capacity; i++) {
            result = result.add(BigInteger.valueOf(9)
                    .multiply(BigInteger.TEN.pow(i - 1))
                    .multiply(BigInteger.valueOf(i)));
        }

        // кол-во предшествующих чисел той же разрядности, что и number
        BigInteger countOfSameCapacityNumbers = number.subtract(BigInteger.valueOf(10).pow(capacity - 1));
        // кол-во символов из предшествующих чисел той же разрядности, что и number
        result = result.add(countOfSameCapacityNumbers.multiply(BigInteger.valueOf(capacity)));
        // поправка на смещение
        result = result.subtract(BigInteger.valueOf(shift - 1));

        System.out.println(result);
    }
}
