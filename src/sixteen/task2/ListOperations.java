package sixteen.task2;

import java.math.BigInteger;
import java.util.List;

/**
 * @author Vitaly Moskalik
 *         created on 10.10.2016
 */
public class ListOperations {

    // инкремент числа, которое представляет данный лист
    public static void incrementList(List<Byte> list) {
        for (int i = list.size() - 1; i >= 0; i--) {
            byte b = list.get(i);
            if (b == 9) {
                list.set(i, (byte) 0);
                if (i == 0) {
                    list.add(0, (byte) 1);
                }
            } else {
                list.set(i, ++b);
                return;
            }
        }
    }

    // декремент числа, которое представляет данный лист
    public static void decrementList(List<Byte> list) {
        for (int i = list.size() - 1; i >= 0; i--) {
            byte b = list.get(i);
            if (b == 0) {
                list.set(i, (byte) 9);
            } else {
                if (i == 0 && b == 1) {
                    list.remove(0);
                } else {
                    list.set(i, --b);
                    return;
                }
            }
        }
    }

    // получить BigInteger из листа
    public static BigInteger listToBigInt(List<Byte> list) {
        StringBuilder sb = new StringBuilder();
        list.forEach(sb::append);
        return new BigInteger(sb.toString());
    }
}
