package sixteen.task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Тропический остров
 *
 * @author Vitaly Moskalik
 *         created on 08.10.2016
 */
public class Island {

    // очередь элементов, которые должны быть обработаны (первый элемент - с наименьшим value)
    private static PriorityQueue<Element> queue = new PriorityQueue<>((el1, el2) -> el1.getValue() - el2.getValue());
    // какие элементы были добавлены в очередь(по индексу)
    private static ArrayList<Boolean> isAdded = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {

            int count = Integer.valueOf(reader.readLine());
            for (int i = 0; i < count; i++) {
                ArrayDTO dto = getArray(reader);
                int answer = check(dto.getIntegers(), dto.getM(), dto.getN());
                System.out.println(answer);
            }
        }
    }

    // считать один остров из stdin
    private static ArrayDTO getArray(BufferedReader reader) throws IOException {
        Scanner scanner;

        scanner = new Scanner(reader.readLine());
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        ArrayDTO dto = new ArrayDTO()
                .setIntegers(new ArrayList<>())
                .setM(m)
                .setN(n);

        for (int i = 0; i < m; i++) {
            scanner = new Scanner(reader.readLine());
            while (scanner.hasNextInt()) {
                dto.getIntegers().add(scanner.nextInt());
            }
        }

        return dto;
    }

    // основной метод проверки
    private static int check(final List<Integer> integers, final int m, final int n) {
        queue.clear();
        isAdded.clear();

        // если хотя бы одна из размерностей меньше 3, возвращаем 0
        if (m < 3 || n < 3) {
            return 0;
        }
        // заполняем isAdded значениями "false"
        for (int i = 0; i < integers.size(); i++) {
            isAdded.add(false);
        }

        addSurfaceElements(integers, m, n);

        return walk(integers, m, n);
    }

    // заполнение очереди внешними элементами
    private static void addSurfaceElements(final List<Integer> integers, final int m, final int n) {
        for (int i = 0; i < m; i++) {
            addToQueue(integers, queue, isAdded, i * n);
            addToQueue(integers, queue, isAdded, i * n + (n - 1));
        }
        for (int i = 1; i < n - 1; i++) {
            addToQueue(integers, queue, isAdded, i);
            addToQueue(integers, queue, isAdded, n * (m - 1) + i);
        }
    }

    // обход острова (графа)
    private static int walk(final List<Integer> integers, final int m, final int n) {
        int threshold = 0;
        int result = 0;

        while (!queue.isEmpty()) {
            Element element = queue.poll();

            int index = element.getIndex();
            if (element.getValue() > threshold) {
                threshold = element.getValue();
            } else {
                result += threshold - element.getValue();
            }

            addAdjacent(integers, m, n, index);
        }
        return result;
    }

    // добавление в очередь смежных элементов
    private static void addAdjacent(final List<Integer> integers, final int m, final int n, final int index) {
        int x = index % n; // столбец элемента
        int y = index / n; // строка элемента
        if (x > 0) {
            addToQueue(integers, queue, isAdded, index - 1);
        }
        if (x < n - 1) {
            addToQueue(integers, queue, isAdded, index + 1);
        }
        if (y > 0) {
            addToQueue(integers, queue, isAdded, index - n);
        }
        if (y < m - 1) {
            addToQueue(integers, queue, isAdded, index + n);
        }
    }

    // добавление элемента в очередь
    private static void addToQueue(final List<Integer> integers,
                                   final PriorityQueue<Element> queue,
                                   final List<Boolean> isAdded,
                                   final int index) {
        if (isAdded.get(index)) {
            return;
        }
        queue.add(new Element()
                .setIndex(index)
                .setValue(integers.get(index)));

        isAdded.set(index, true);
    }
}
