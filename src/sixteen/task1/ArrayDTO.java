package sixteen.task1;

import java.util.List;

/**
 * @author Vitaly Moskalik
 *         created on 08.10.2016
 */
public class ArrayDTO {

    private List<Integer> integers; //исходный массив в виде листа
    private Integer m; //строки
    private Integer n; //столбцы

    public List<Integer> getIntegers() {
        return integers;
    }

    public ArrayDTO setIntegers(List<Integer> integers) {
        this.integers = integers;
        return this;
    }

    public Integer getM() {
        return m;
    }

    public ArrayDTO setM(Integer m) {
        this.m = m;
        return this;
    }

    public Integer getN() {
        return n;
    }

    public ArrayDTO setN(Integer n) {
        this.n = n;
        return this;
    }
}
