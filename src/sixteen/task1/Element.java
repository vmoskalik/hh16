package sixteen.task1;

/**
 * @author Vitaly Moskalik
 *         created on 09.10.2016
 */
public class Element {

    private int index;
    private int value;

    public int getIndex() {
        return index;
    }

    public Element setIndex(int index) {
        this.index = index;
        return this;
    }

    public int getValue() {
        return value;
    }

    public Element setValue(int value) {
        this.value = value;
        return this;
    }
}
